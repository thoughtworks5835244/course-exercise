package org.example;

import java.util.ArrayList;
import java.util.List;

public class MultiplicationTableTutor {

    public String buildMultiplicationTable(int minFactor, int maxFactor) {
        return isValid(minFactor, maxFactor)
            ? format(generateLines(minFactor, maxFactor))
            : null;
    }

    private boolean isValid(int minFactor, int maxFactor) {
        return isInRange(minFactor) &&
            isInRange(maxFactor) &&
            isMinLessThanOrEqualToMax(minFactor, maxFactor);
    }

    private boolean isInRange(int factor) {
        return factor >= 1 && factor <= 1000;
    }

    private boolean isMinLessThanOrEqualToMax(int minFactor, int maxFactor) {
        return minFactor <= maxFactor;
    }

    private String format(List<Line> lines) {
        var table = new StringBuilder();

        for (Line line : lines) {
            var row = new StringBuilder();
            for (Expression expression : line.getExpressions()) {
                row.append(String.format(
                    "%d * %d = %2d  ",
                    expression.getMultiplier(),
                    expression.getMultiplicand(),
                    expression.getProduct()
                ));
            }
            table.append(row.append("\n"));
        }

        return table.toString();
    }

    private List<Line> generateLines(int minFactor, int maxFactor) {
        var lines = new ArrayList<Line>();

        for (int i = minFactor; i <= maxFactor; i++) {
            lines.add(new Line(generateLine(minFactor, i), minFactor, i));
        }

        return lines;
    }

    private List<Expression> generateLine(int startMultiplier, int fixedMultiplicand) {
        var expressions = new ArrayList<Expression>();

        for (int i = startMultiplier; i <= fixedMultiplicand; i++) {
            expressions.add(new Expression(i, fixedMultiplicand, i * fixedMultiplicand));
        }

        return expressions;
    }

    private static class Expression {

        public Expression(int multiplier, int multiplicand, int product) {
            this.multiplier = multiplier;
            this.multiplicand = multiplicand;
            this.product = product;
        }

        public int getMultiplier() {
            return multiplier;
        }

        public int getMultiplicand() {
            return multiplicand;
        }

        public int getProduct() {
            return product;
        }

        private int multiplier;
        private int multiplicand;
        private int product;
    }

    private static class Line {

        public Line(List<Expression> expressions, int startMultiplier, int fixedMultiplicand) {
            this.expressions = expressions;
            this.startMultiplier = startMultiplier;
            this.fixedMultiplicand = fixedMultiplicand;
        }

        public List<Expression> getExpressions() {
            return expressions;
        }

        public int getStartMultiplier() {
            return startMultiplier;
        }

        public int getFixedMultiplicand() {
            return fixedMultiplicand;
        }

        private List<Expression> expressions;
        private int startMultiplier;
        private int fixedMultiplicand;
    }
}
