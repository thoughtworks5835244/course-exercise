package org.example;

public class MultiplicationTable {

    public String print(int start, int end) {
        return isValid(start, end) ? printTable(start, end) : null;
    }

    private boolean isValid(int start, int end) {
        return start >= 1 && end >= 1 && end <= 1000 && start <= end;
    }

    private String printTable(int start, int end) {
        var table = new StringBuilder();

        for (int i = start; i <= end; i++) {
            table.append(printLine(start, i));
        }

        return table.toString().trim();
    }

    private String printLine(int startMultiplier, int multiplicand) {
        var line = new StringBuilder();

        for (int i = startMultiplier; i <= multiplicand; i++) {
            line.append(String.format("%d * %d = %2d  ", i, multiplicand, i * multiplicand));
        }

        return line.toString().trim() + "\n";
    }
}
