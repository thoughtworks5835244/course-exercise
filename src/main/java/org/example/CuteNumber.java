package org.example;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class CuteNumber {

    public boolean checkCuteNumber(List<Integer> numbers) {
        var evenNumbers = findAllEvenNumbers(numbers);
        var largestNumber = findLargestNumber(evenNumbers);
        return isGreaterThan10(largestNumber);
    }

    public List<Integer> findAllEvenNumbers(List<Integer> numbers) {
        return numbers.stream().filter(n -> n % 2 == 0).collect(toList());
    }

    public Integer findLargestNumber(List<Integer> numbers) {
        return numbers.stream().mapToInt(Integer::intValue).max().orElse(0);
    }

    public boolean isGreaterThan10(Integer number) {
        return number > 10;
    }
}
